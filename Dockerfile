FROM nginx:alpine

WORKDIR /usr/share/nginx/html
COPY *.html .
COPY js js
COPY css css
COPY img img
COPY test.json .

EXPOSE 80
